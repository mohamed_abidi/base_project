/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package take.your.ticket;

/**
 *
 * @author Mohamed
 */
public class payement extends javax.swing.JInternalFrame {

    /**
     * Creates new form payement
     */
    public payement() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblPaymentMethod = new javax.swing.JLabel();
        rbCash = new javax.swing.JRadioButton();
        rbCreditCard = new javax.swing.JRadioButton();
        lblCCNo = new javax.swing.JLabel();
        txtCCNo = new javax.swing.JTextField();
        lblCCType = new javax.swing.JLabel();
        combCCType = new javax.swing.JComboBox<>();
        lblCCHolder = new javax.swing.JLabel();
        txtCCHolder = new javax.swing.JTextField();
        lblCCBank = new javax.swing.JLabel();
        txtCCBank = new javax.swing.JTextField();
        btnPrintTransaction = new javax.swing.JButton();
        btnCTransaction = new javax.swing.JButton();

        lblPaymentMethod.setText("Payment Method :");

        rbCash.setSelected(true);
        rbCash.setText("Cash");
        rbCash.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbCashMouseClicked(evt);
            }
        });

        rbCreditCard.setText("Credit Card");
        rbCreditCard.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbCreditCardMouseClicked(evt);
            }
        });

        lblCCNo.setText("Credit Card No :");
        lblCCNo.setToolTipText("CreditCard Number");

        txtCCNo.setToolTipText("(10 Digits)");

        lblCCType.setText("Type :");

        combCCType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Master Card", "Visa", "American Express", "Other" }));

        lblCCHolder.setText("Holder :");

        txtCCHolder.setToolTipText("CreditCard Holder Name");

        lblCCBank.setText("Bank :");

        btnPrintTransaction.setIcon(new javax.swing.ImageIcon(getClass().getResource("/take/your/ticket/IconPrint.png"))); // NOI18N
        btnPrintTransaction.setText("Print Transaction");
        btnPrintTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintTransactionActionPerformed(evt);
            }
        });

        btnCTransaction.setIcon(new javax.swing.ImageIcon(getClass().getResource("/take/your/ticket/IconPayment.png"))); // NOI18N
        btnCTransaction.setText("Complete Transaction");
        btnCTransaction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCTransactionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblPaymentMethod)
                        .addGap(18, 18, 18)
                        .addComponent(rbCash)
                        .addGap(18, 18, 18)
                        .addComponent(rbCreditCard))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblCCNo)
                        .addGap(18, 18, 18)
                        .addComponent(txtCCNo, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCCType)))
                .addGap(18, 18, 18)
                .addComponent(combCCType, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblCCHolder)
                .addGap(18, 18, 18)
                .addComponent(txtCCHolder, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(lblCCBank)
                .addGap(18, 18, 18)
                .addComponent(txtCCBank, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(156, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrintTransaction)
                .addGap(23, 23, 23)
                .addComponent(btnCTransaction)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPaymentMethod)
                    .addComponent(rbCash)
                    .addComponent(rbCreditCard))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCCNo)
                    .addComponent(txtCCNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCCType)
                    .addComponent(lblCCHolder)
                    .addComponent(txtCCHolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCCBank)
                    .addComponent(txtCCBank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(combCCType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrintTransaction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCTransaction))
                .addGap(58, 58, 58))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbCashMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbCashMouseClicked
        this.txtCCNo.setEnabled(false);
        this.combCCType.setEnabled(false);
        this.txtCCHolder.setEnabled(false);
        this.txtCCBank.setEnabled(false);
        this.lblCCBank.setEnabled(false);
        this.lblCCHolder.setEnabled(false);
        this.lblCCNo.setEnabled(false);
        this.lblCCType.setEnabled(false);
    }//GEN-LAST:event_rbCashMouseClicked

    private void rbCreditCardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbCreditCardMouseClicked
        this.txtCCNo.setEnabled(true);
        this.combCCType.setEnabled(true);
        this.txtCCHolder.setEnabled(true);
        this.txtCCBank.setEnabled(true);
        this.lblCCBank.setEnabled(true);
        this.lblCCHolder.setEnabled(true);
        this.lblCCNo.setEnabled(true);
        this.lblCCType.setEnabled(true);
    }//GEN-LAST:event_rbCreditCardMouseClicked

    private void btnPrintTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintTransactionActionPerformed
        try {
            JasperDesign jd = JRXmlLoader.load("reports\\paymentReport_All.jrxml");
            String sql = "select p.paymentID as 'Payment ID', p.method as 'Payment Method', p.payAmnt as 'Amount', p.date as 'Payment Date', t.ticketID as 'Ticket ID', t.custID as 'Customer ID', t.flightID as 'Flight ID' from payment_info p left outer join creditcard_info c on p.paymentID=c.paymentID, ticket_info t where t.paymentID=p.paymentID and p.paymentID='"+paymentID+"'";
            JRDesignQuery newQuery = new JRDesignQuery();
            newQuery.setText(sql);
            jd.setQuery(newQuery);
            JasperReport jr = JasperCompileManager.compileReport(jd);
            JasperPrint jp = JasperFillManager.fillReport(jr, null,dbcon);
            JasperViewer.viewReport(jp,false);

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error occured while generating transaction report.","Error Occured",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPrintTransactionActionPerformed

    private void btnCTransactionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCTransactionActionPerformed
        if(this.rbCreditCard.isSelected()){
            if(cktxtCCNo() & cktxtCCHolder() & cktxtCCBank()){
                int userChoice = JOptionPane.showConfirmDialog(null,"Are you sure details are correct?", "Confirm Details", JOptionPane.YES_NO_OPTION);

                if(userChoice==0){
                    //Adding payment info to database
                    String save_payment_info_SQL = "insert into payment_info values(?,?,?,?)";
                    String save_ticket_info_SQL = "insert into ticket_info values(?,?,?,?,?,?,?)";
                    String save_creditcard_info_SQL = "insert into creditcard_info values(?,?,?,?,?)";
                    paymentID = generatePaymentID();

                    try{
                        PreparedStatement pst = dbcon.prepareStatement(save_payment_info_SQL);
                        PreparedStatement pst2 = dbcon.prepareStatement(save_creditcard_info_SQL);
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        Date date = new Date();
                        String d = dateFormat.format(date);
                        pst.setString(1, paymentID);
                        pst.setString(2, "CreditCard");
                        pst.setString(3, this.lblTotalPrice.getText());
                        pst.setString(4, d);
                        pst.executeUpdate();

                        pst2.setString(1, paymentID);
                        pst2.setString(2, this.txtCCNo.getText());
                        pst2.setString(3, this.combCCType.getSelectedItem().toString());
                        pst2.setString(4, this.txtCCHolder.getText());
                        pst2.setString(5, this.txtCCBank.getText());
                        pst2.executeUpdate();
                        increasePaymentIDByOne();
                    }
                    catch(Exception exc){
                        JOptionPane.showMessageDialog(null, "Database connection failed while saving information","Database Connection Error",JOptionPane.ERROR_MESSAGE);
                    }
                    int ntickets=Integer.parseInt(this.spinNTickets.getValue().toString());
                    //Adding tickets to the database
                    try{
                        PreparedStatement pst = dbcon.prepareStatement(save_ticket_info_SQL);

                        while(ntickets>0){
                            pst.setString(1, generateTicketID());
                            pst.setString(2, this.txtFlightID.getText());
                            pst.setString(3, this.txtCustID.getText());
                            pst.setString(4, this.combClass.getSelectedItem().toString());
                            pst.setString(5, this.lblTicketPrice.getText());
                            pst.setString(6, paymentID);
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            String date = df.format(txtFlightDate.getDate());
                            pst.setString(7, date);
                            pst.executeUpdate();
                            increaseTicketIDByOne();
                            ntickets--;
                        }
                        JOptionPane.showMessageDialog(null, "Transaction Completed.","Trasnsaction Successful.",JOptionPane.INFORMATION_MESSAGE);
                        //------------------------------------
                        this.btnPrintTransaction.setEnabled(true);
                        this.btnCTransaction.setEnabled(false);
                        this.btnEdit.setEnabled(false);
                        this.rbCash.setEnabled(false);
                        this.rbCreditCard.setEnabled(false);
                        this.txtCCNo.setEnabled(false);
                        this.combCCType.setEnabled(false);
                        this.txtCCHolder.setEnabled(false);
                        this.txtCCBank.setEnabled(false);
                        this.lblPaymentMethod.setEnabled(false);
                        this.lblCCBank.setEnabled(false);
                        this.lblCCHolder.setEnabled(false);
                        this.lblCCType.setEnabled(false);
                        this.lblCCNo.setEnabled(false);
                        this.btnCTransaction.setEnabled(false);
                        //------------------------------------
                    }
                    catch(Exception exc){
                        exc.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Database connection failed while adding tickets","Database Connection Error",JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
            else{
                JOptionPane.showMessageDialog(null, "Incorrect CreditCard Information","Error",JOptionPane.ERROR_MESSAGE);
            }
        }
        else if(this.rbCash.isSelected()){
            int userChoice = JOptionPane.showConfirmDialog(null,"Are you sure details are correct?", "Confirm Details", JOptionPane.YES_NO_OPTION);

            if(userChoice==0){
                //Adding payment info to database
                String save_payment_info_SQL = "insert into payment_info values(?,?,?,?)";
                String save_ticket_info_SQL = "insert into ticket_info values(?,?,?,?,?,?,?)";
                paymentID = generatePaymentID();
                increasePaymentIDByOne();
                try{
                    PreparedStatement pst = dbcon.prepareStatement(save_payment_info_SQL);

                    pst.setString(1, paymentID);
                    pst.setString(2, "Cash");
                    pst.setString(3, this.lblTotalPrice.getText());
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    String d = dateFormat.format(date);
                    pst.setString(4, d);
                    pst.executeUpdate();
                }
                catch(Exception exc){
                    JOptionPane.showMessageDialog(null, "Database connection failed while saving information","Database Connection Error",JOptionPane.ERROR_MESSAGE);
                }

                int ntickets=Integer.parseInt(this.spinNTickets.getValue().toString());
                //Adding tickets to the database
                try{
                    PreparedStatement pst = dbcon.prepareStatement(save_ticket_info_SQL);
                    while(ntickets>0){
                        pst.setString(1, generateTicketID());
                        pst.setString(2, this.txtFlightID.getText());
                        pst.setString(3, this.txtCustID.getText());
                        pst.setString(4, this.combClass.getSelectedItem().toString());
                        pst.setString(5, this.lblTicketPrice.getText());
                        pst.setString(6, paymentID);
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        String date = df.format(txtFlightDate.getDate());
                        pst.setString(7, date);
                        pst.executeUpdate();
                        increaseTicketIDByOne();
                        ntickets--;
                    }
                    JOptionPane.showMessageDialog(null, "Transaction Completed. ","Trasnsaction Successful.",JOptionPane.INFORMATION_MESSAGE);
                    //-------------------------------------------
                    this.btnPrintTransaction.setEnabled(true);
                    this.btnCTransaction.setEnabled(false);
                    this.btnEdit.setEnabled(false);
                    this.rbCash.setEnabled(false);
                    this.rbCreditCard.setEnabled(false);
                    //--------------------------------------------
                }
                catch(Exception exc){
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Database connection failed while saving information","Database Connection Error",JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }//GEN-LAST:event_btnCTransactionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCTransaction;
    private javax.swing.JButton btnPrintTransaction;
    private javax.swing.JComboBox<String> combCCType;
    private javax.swing.JLabel lblCCBank;
    private javax.swing.JLabel lblCCHolder;
    private javax.swing.JLabel lblCCNo;
    private javax.swing.JLabel lblCCType;
    private javax.swing.JLabel lblPaymentMethod;
    private javax.swing.JRadioButton rbCash;
    private javax.swing.JRadioButton rbCreditCard;
    private javax.swing.JTextField txtCCBank;
    private javax.swing.JTextField txtCCHolder;
    private javax.swing.JTextField txtCCNo;
    // End of variables declaration//GEN-END:variables
}
